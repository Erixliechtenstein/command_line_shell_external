#include "0_UTILITIES/Includes.h"
#include COMMAND_LINE_INTERFACE


void settings::populate_available_usernames_and_passwords(){

usernames_passwords_privileges["admin"] = user("adminpassword");

std::map<std::string,int> normaluser_permissions;
normaluser_permissions["LOGIN"] = 0;

usernames_passwords_privileges["normaluser"] = user("normalpassword",normaluser_permissions);

if (use_admin_permissions_by_default) {active_user = "admin";} else {active_user = "normaluser";}
}

