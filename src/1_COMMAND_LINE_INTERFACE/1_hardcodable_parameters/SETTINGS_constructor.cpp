#include "0_UTILITIES/Includes.h"
#include COMMAND_LINE_INTERFACE

settings::settings(){

quit_flag = false;
require_password_on_startup = true;
use_admin_permissions_by_default = true;

populate_available_usernames_and_passwords();
initialise_command_tree();

}


