#include "0_UTILITIES/Includes.h"
#include COMMAND_LINE_INTERFACE
#include UTILITIES
#include DEFINES


void cli::exit_program(command_info_container* info_to_pass){
    settings::get().quit_flag = true;
    std::cout << "Exited program..press any key to close window\n";
}


void cli::display_welcome_message(command_info_container* p){std::cout << " \n                           *** Welcome! *** \n\n";}
void cli::display_completion_message(command_info_container* p){std::cout << "Finished running..\n";}

