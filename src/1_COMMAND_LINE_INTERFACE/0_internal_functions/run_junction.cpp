#include "0_UTILITIES/Includes.h"
#include COMMAND_LINE_INTERFACE
#include DEFINES

void cli::run_command_junction(COMMAND_JUNCTION_KEY key,command_info_container* user_data){
if(settings::get().quit_flag) {return;}
settings::get().previous_junction = settings::get().previous_junction;
settings::get().current_junction = key;

if (key == COMMAND_JUNCTION_KEY::C_GO_BACK) {
    run_command_junction(settings::get().previous_junction,user_data);
} else {

display_available_commands(key);
std::string cmd_text = prompt_for_command(EMPTY_STRING);
command cmd = settings::get().match_input_text_to_a_command(cmd_text,key);


if (cmd.func_to_execute != nullptr) {cmd.func_to_execute(user_data);
if (cmd.next_func_to_execute != nullptr) {cmd.next_func_to_execute(user_data);}
} else
    {std::cout << "invalid command entered...try again"; nextline;
    run_command_junction(settings::get().current_junction,user_data);
    }

}

}
