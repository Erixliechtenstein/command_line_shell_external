#include "0_UTILITIES/Includes.h"
#include COMMAND_LINE_INTERFACE
#include DEFINES
#include UTILITIES

void settings::add_auto_commands_to_junction(COMMAND_JUNCTION_KEY key){
std::vector<command> v = permissioned_command_tree[key];
ptr_to_command_func funcname;
if (key != COMMAND_JUNCTION_KEY::C_MAIN && key != COMMAND_JUNCTION_KEY::C_CHANGE_USER) {

    funcname = &cli::run_command_junction_C_GO_BACK;
    auto it = find_if(v.begin(), v.end(), [&funcname](const command& cmd) {return (cmd.func_to_execute == funcname);});
    if (it == v.end()) {

   permissioned_command_tree[key].push_back(cmd_J1_go_back);
    }

        funcname = &cli::run_command_junction_C_MAIN;
    it = find_if(v.begin(), v.end(), [&funcname](const command& cmd) {return (cmd.func_to_execute == funcname);});
    if (it == v.end()) {

   permissioned_command_tree[key].push_back(cmd_J0_main_menu);
    }

}


if (key != COMMAND_JUNCTION_KEY::C_QUIT) {
    auto it = find_if(v.begin(), v.end(), [&](const command& cmd) {return (cmd.func_to_execute == &cli::run_command_junction_C_QUIT);});
            if ( it == v.end()) {permissioned_command_tree[key].push_back(cmd_J1_quit_confirmation);}
}
}
