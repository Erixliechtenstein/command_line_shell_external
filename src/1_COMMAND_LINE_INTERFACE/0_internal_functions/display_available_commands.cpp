#include "0_UTILITIES/Includes.h"
#include COMMAND_LINE_INTERFACE
#include DEFINES
#include UTILITIES

#include <iomanip>


void cli::display_available_commands(COMMAND_JUNCTION_KEY key){

std::cout << "\nAvailable commands: "; nextline;
for (int i = 0; i < settings::get().permissioned_command_tree[key].size();++i){

std::cout << i << ") " << std::setw(10) << settings::get().permissioned_command_tree[key][i].text_of_command[0] << vlahs
<< std::setw(30) << settings::get().permissioned_command_tree[key][i].description_of_command << vlahs; nextline;
} nextline;

}

