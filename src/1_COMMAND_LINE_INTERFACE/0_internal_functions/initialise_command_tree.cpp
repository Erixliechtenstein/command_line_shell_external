#include "0_UTILITIES/Includes.h"
#include COMMAND_LINE_INTERFACE
#include ACTUAL_APP_1
#include DEFINES
#include UTILITIES

void settings::initialise_command_tree(){

command_storage.clear();
command cmd_continue_with_default_user("CONTD","Continue with default user",&cli::run_command_junction_C_MAIN);
cmd_login = command("LOGIN","Change user",&cli::run_authorisation);

command cmd_F_exit_program("FINALQUIT","Exit for good",&cli::exit_program);
cmd_J0_main_menu = command ("MAIN","Revert to main menu",&cli::run_command_junction_C_MAIN);
command cmd_J1_test_snippet("SNPT","Test a code snippet",&cli::test_code_snippet);
cmd_J1_go_back = command("GOBACK","Go back",&cli::run_command_junction_C_GO_BACK);

command cmd_J1_prompt_for_test_inputs("RTST","Input data to run Test",&cli::run_command_junction_C_TEST);

cmd_J1_quit_confirmation = command("EXIT","Confirm exiting program",&cli::run_command_junction_C_QUIT);

command cmd_F0_J1_test_manual_input("TSTM","Test - input via console",&cli::prompt_for_single_test_inputs,&cli::run_test);
command cmd_F1_J1_test_from_xls("TXLS","Test - input via xls",&cli::load_test_input_xls,&cli::run_test);
command cmd_F2_J1_test_from_csv("TCSV","Test - input via csv",&cli::load_test_input_csv,&cli::run_test);

command_storage[COMMAND_JUNCTION_KEY::C_MAIN].push_back(cmd_J1_quit_confirmation);
command_storage[COMMAND_JUNCTION_KEY::C_MAIN].push_back(cmd_J1_test_snippet);
command_storage[COMMAND_JUNCTION_KEY::C_MAIN].push_back(cmd_J1_prompt_for_test_inputs);
command_storage[COMMAND_JUNCTION_KEY::C_MAIN].push_back(cmd_login);

command_storage[COMMAND_JUNCTION_KEY::C_QUIT].push_back(cmd_F_exit_program);

command_storage[COMMAND_JUNCTION_KEY::C_TEST].push_back(cmd_F0_J1_test_manual_input);
command_storage[COMMAND_JUNCTION_KEY::C_TEST].push_back(cmd_F1_J1_test_from_xls);
command_storage[COMMAND_JUNCTION_KEY::C_TEST].push_back(cmd_F2_J1_test_from_csv);

command_storage[COMMAND_JUNCTION_KEY::C_CHANGE_USER].push_back(cmd_continue_with_default_user);
command_storage[COMMAND_JUNCTION_KEY::C_CHANGE_USER].push_back(cmd_login);

apply_permissions_to_command_availability();

for (auto& command_keys: permissioned_command_tree) { // prnt "looping thru " << command_keys.first << vlahs;
    add_auto_commands_to_junction(command_keys.first);
    add_numerical_command_invokes(command_keys.first);
}

}

