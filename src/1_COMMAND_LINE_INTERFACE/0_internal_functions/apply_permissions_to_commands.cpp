#include "0_UTILITIES/Includes.h"
#include COMMAND_LINE_INTERFACE
#include DEFINES
#include UTILITIES

void settings::apply_permissions_to_command_availability(){
permissioned_command_tree = command_storage;

for (auto& command_permission: usernames_passwords_privileges[active_user].command_permissions) {

if (command_permission.second == 0) {
    for (auto& command_keys: permissioned_command_tree) {

    auto it = find_if(command_keys.second.begin(), command_keys.second.end(), [&command_permission](const command& cmd)
                      {return (utilities::does_vector_contain(cmd.text_of_command,command_permission.first)); });
                        if (it!= command_keys.second.end()){command_keys.second.erase(it);}
}
}
}

}

