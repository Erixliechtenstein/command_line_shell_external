#include "0_UTILITIES/Includes.h"
#include COMMAND_LINE_INTERFACE
#include DEFINES
#include UTILITIES

std::string cli::prompt_for_command(std::string msg){

std::string type_command;

if (!utilities::is_string_empty_or_undefined(msg)) {std::cout << msg<< ":"; nextline;} else{
prnt "please enter command: ";
}

std::getline(std::cin, type_command);
return type_command;
}
