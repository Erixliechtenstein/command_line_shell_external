#include "0_UTILITIES/Includes.h"
#include COMMAND_LINE_INTERFACE
#include UTILITIES


command settings::match_input_text_to_a_command(std::string cmd,COMMAND_JUNCTION_KEY key){
bool search_complete = false;

command C;

while (!search_complete) {

for (int i = 0; i < permissioned_command_tree[key].size(); ++i) {
    for (int j = 0; j < permissioned_command_tree[key][i].text_of_command.size(); ++j) {
    if (utilities::to_lower_case(cmd) == utilities::to_lower_case(permissioned_command_tree[key][i].text_of_command[j])) {
            C = permissioned_command_tree[key][i]; search_complete = true; break;}
    }
}
        search_complete = true;
}

return C;
}

