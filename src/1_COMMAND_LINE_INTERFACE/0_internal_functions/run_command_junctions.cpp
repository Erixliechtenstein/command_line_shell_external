#include "0_UTILITIES/Includes.h"
#include COMMAND_LINE_INTERFACE
#include UTILITIES
#include DEFINES

void cli::run_command_junction_C_MAIN(command_info_container* info_to_pass){
    nextline; prnt "**********************************************************************"; nextline;
    run_command_junction(COMMAND_JUNCTION_KEY::C_MAIN,info_to_pass);
}

void cli::run_command_junction_C_QUIT(command_info_container* info_to_pass){
    run_command_junction(COMMAND_JUNCTION_KEY::C_QUIT,info_to_pass);
}

void cli::run_command_junction_C_GO_BACK(command_info_container* info_to_pass){
    run_command_junction(COMMAND_JUNCTION_KEY::C_GO_BACK,info_to_pass);
}

void cli::run_command_junction_C_CHANGE_USER(command_info_container* info_to_pass){
    run_command_junction(COMMAND_JUNCTION_KEY::C_CHANGE_USER,info_to_pass);
}

void cli::run_command_junction_C_TEST(command_info_container* info_to_pass){
    /// welcome msg
    run_command_junction(COMMAND_JUNCTION_KEY::C_TEST,info_to_pass);
    /// completion msg
}
