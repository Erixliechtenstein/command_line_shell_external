#include "0_UTILITIES/Includes.h"
#include COMMAND_LINE_INTERFACE


bool settings::perform_authorisation(std::string username,std::string password){
    if (usernames_passwords_privileges[username].password == password)
        {nextline; prnt username << " AUTHORISED";nextline; return true;}
    else
        {nextline; prnt username << " NOT AUTHORISED. Please try again"; nextline; return false;}
    }


void cli::run_authorisation(command_info_container* user_data){

std::string username;
std::string password;
username = cli::prompt_for_command("Please enter username");
password = cli::prompt_for_command("Please enter password");

if (!settings::get().perform_authorisation(username,password)){run_authorisation(user_data);} else {settings::get().set_active_user(username);}

}

void settings::set_active_user(std::string username){
    active_user = username;
    initialise_command_tree();
}
