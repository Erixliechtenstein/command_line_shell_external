#include "0_UTILITIES/Includes.h"
#include ACTUAL_APP_1
#include COMMAND_LINE_INTERFACE
#include UTILITIES
#include DEFINES


void cli::prompt_for_single_test_inputs(command_info_container* info_to_pass){

test_input* t = new test_input;

utilities::get_value_from_user("enter val 1 - variable 1 - int",t->intvar);
utilities::get_value_from_user("enter val 2 - variable 2 - double",t->q);
utilities::get_value_from_user("enter val 3 - variable 3 - string",t->stringvar);

info_to_pass->container_1 = t;

}


void cli::load_test_input_csv(command_info_container* info_to_pass){

prnt "loaded input from csv.."; nextline;
}

void cli::load_test_input_xls(command_info_container* info_to_pass){

prnt "loaded input from xls.."; nextline;
}

void cli::run_test(command_info_container* info_to_pass){

test_input* pt = static_cast<test_input*>(info_to_pass->container_1);
test_input t = *pt;

prnt "in RUN_TEST -- retrieving collected inputs -- "; nextline;
prnt t.q << vlahs << t.intvar << vlahs << t.stringvar; nextline;
test_output T = tester::test(t);
prnt "done running test with collected input..";
}

