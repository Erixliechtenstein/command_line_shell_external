#include "0_UTILITIES/Includes.h"
#include UTILITIES

std::string utilities::get_right_of_last_delim(const std::string& str,const std::string& delim){
return str.substr(str.find_last_of(delim) + delim.size());
}


std::vector<std::string> utilities::split(const std::string& str, const std::string& delimiter){

    std::vector<std::string>result;

    size_t pos = 0;
std::string s = str;
while ((pos = s.find(delimiter)) != std::string::npos) {
    result.push_back(s.substr(0, pos));
    s.erase(0, pos + delimiter.length());
}
result.push_back(s);
    return result;
}


std::string utilities::replace_string(std::string container, const std::string& what_to_replace,const std::string& replacement) {
    size_t pos = 0;
    while ((pos = container.find(what_to_replace, pos)) != std::string::npos) {
         container.replace(pos, what_to_replace.length(), replacement);
         pos += replacement.length();
    }
    return container;
}


std::string utilities::to_lower_case(const std::string& uppercase){
     std::string lowercase = uppercase;
     std::transform(lowercase.begin(), lowercase.end(), lowercase.begin(), ::tolower);
        return lowercase;
        }


std::string utilities::cut_slash_from_dir(const std::string& dir){
    std::string d = dir;
if ((d.back() == FWDSLASH) || (d.back() == BACKSLASH))  {d = d.substr(0, d.size()-1);}
if ((d.back() == FWDSLASH) || (d.back() == BACKSLASH))  {d = d.substr(0, d.size()-1);}
if ((d.back() == FWDSLASH) || (d.back() == BACKSLASH))  {d = d.substr(0, d.size()-1);}
    return d;
    }


bool        utilities::does_vector_contain(const std::vector<std::string>& container, const std::string& containee){

auto it = std::find(container.begin(),container.end(),containee);
return (it != container.end());
}


std::string utilities::cut_one_folder_level_from_dir(const std::string& dir) {
    std::string d = cut_slash_from_dir(dir);
    d = dir.substr(0, d.rfind("/"));
return d;
}


bool utilities::contains_string(const std::string& container, const std::string& containee){
    if (container.find(containee) != std::string::npos) {return true;} else {return false;}
}


bool utilities::is_string_empty(const std::string& s){
    bool is_all_whitespaces = (s.find_first_not_of(' ') == std::string::npos);
    if (s == " " || s == EMPTY_STRING || s.empty() || is_all_whitespaces) {return true;} else {return false;}
}


bool utilities::is_string_empty_or_undefined(const std::string& s){
    if (is_string_empty(s) || s == UNDEFINED_STRING) {return true;} else {return false;}
}


bool utilities::does_file_exist(const std::string& full_file_path) {
    struct stat buffer;
    return (stat (full_file_path.c_str(), &buffer) == 0);
    }


bool utilities::does_directory_exist(const std::string& folder_name) {
    std::string f_n = folder_name; if (folder_name.back() != BACKSLASH && folder_name.back() != FWDSLASH) {f_n = folder_name + FWDSLASH;}
    struct stat buffer; return (stat(f_n.c_str(), &buffer) == 0 && S_ISDIR(buffer.st_mode));
    }


std::string utilities::current_directory() {
    const unsigned long maxDir = 260;
    std::string whitespaces ("\t\f\v\n\r");
    char currentDir[maxDir];
    GetCurrentDirectory(maxDir, currentDir);
    std::string settings_directory(currentDir);
    settings_directory = settings_directory.substr(0,settings_directory.find_last_not_of(whitespaces)+1) + "/";
    std::replace(settings_directory.begin(),settings_directory.end(),('\\'),'/');
    return settings_directory;
}


std::string utilities::current_date_time(){
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y_%m_%d_%X", &tstruct);

    std::string f = std::string(buf);
f.erase(std::remove(f.begin(), f.end(), ':'), f.end());
    return f;
}


std::string utilities::create_ffp(const std::string& file_name_or_folder, const std::string& dir_level_1, const std::string& dir_level_2,const std::string& dir_level_3){

    std::string part1; std::string part2 = "";std::string part3 = "";

    part1 = utilities::cut_slash_from_dir(dir_level_1) + FWDSLASH;
    if (!is_string_empty_or_undefined(dir_level_2)) {part2 = utilities::cut_slash_from_dir(dir_level_2) + FWDSLASH;}
    if (!is_string_empty_or_undefined(dir_level_3)) {part3 = utilities::cut_slash_from_dir(dir_level_3) + FWDSLASH;}

    std::string ffp =   part1 + part2 + part3 + file_name_or_folder;
    return ffp;
}


std::string utilities::find_file_by_foldername_and_filename(const std::string& full_file_name, const std::string& folder, bool use_current_dir_as_search_base, const std::string& override_dir){

std::string d,ffp;
if (use_current_dir_as_search_base) {d = utilities::current_directory();} else {d = override_dir;}

for (int i = 0; i < 6; ++i) {
        ffp = utilities::create_ffp(full_file_name,d,folder);
        if (utilities::does_file_exist(ffp)) {break;} else {d = utilities::cut_one_folder_level_from_dir(d);}
}

return ffp;
}


std::string utilities::find_folder_by_foldername(std::string folder, bool use_current_dir_as_search_base,std::string override_dir) {

std::string d, ffp;

if (use_current_dir_as_search_base) {d = utilities::current_directory();} else {d = override_dir;}

for (int i = 0; i < 4; ++i) {
        ffp = utilities::create_ffp(folder,d);
    if (utilities::does_directory_exist(ffp)) {break;} else {d = utilities::cut_one_folder_level_from_dir(d);}
}

return ffp;
}


void utilities::stn(int& val,const std::string& Text){
    try {std::string t = Text; if (is_string_empty(Text)) {t = "0";} std::stringstream ss(t); ss >> val;}
    catch(std::exception& e) {std::cout << e.what() << '\n';}
    }

void utilities::stn(float& val,const std::string& Text){
    try {std::string t = Text; if (is_string_empty(Text)) {t = "0";} std::stringstream ss(t); ss >> val;}
    catch(std::exception& e) {std::cout << e.what() << '\n';}
    }

void utilities::stn(double& val,const std::string& Text){
    try {std::string t = Text; if (is_string_empty(Text)) {t = "0";} std::stringstream ss(t); ss >> val;}
    catch(std::exception& e) {std::cout << e.what() << '\n';}
    }

void utilities::stn(bool& val,const std::string& Text){
        try {
        std::string t = Text; if (t == "1" || to_lower_case(t) == "true"){val = true;} else {val = false;}
            }
    catch(std::exception& e) {std::cout << e.what() << '\n';}
    }

void utilities::stn(std::string& val,const std::string& Text){
       try {std::string t = Text; std::stringstream ss(t); ss >> val;} catch(std::exception& e) {std::cout << e.what() << '\n';}
    }
