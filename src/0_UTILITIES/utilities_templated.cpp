#ifdef UTILITIES

template<typename T> void get_vector_element(T save_to, int i, const std::vector<T>& v){
    if (i <= (v.size()-1) && i > 0) {save_to = v[i];} else {std::cout << "ERROR: attempting to access invalid vector index";}
}


template<typename T> int utilities::find_nth_substring(int n, const std::basic_string<T>& container,const std::basic_string<T>& query,bool repeats) {
   std::string::size_type i = container.find(query);
   std::string::size_type adv = (repeats) ? 1 : query.length();
   int j; for (j = 1; j < n && i != std::basic_string<T>::npos; ++j) {i = container.find(query, i+adv);}
   if (j == n) {return(i);}else {return(-1);}
}


template <typename T> void utilities::parse(T val, std::string varname, const std::vector<std::string>& line,const int id){
        int i = id; std::string Text;
        if (id != UNDEFINED_INT) {get_vector_element(Text,i,line);} else
        {
            auto it = find_if(line.begin(), line.end(), [&varname](const std::string t) {return contains_string(t,varname);});
            i = std::distance(line.begin(),it);
            get_vector_element(Text,i,line);
        }

        if (contains_string(Text,"=")) {Text = get_right_of_last_delim(Text,"=");}
        stn(val,Text);
        }


template <typename T> void utilities::retrieve_void_ptr_val(T& val, void* v_ptr){

val = *static_cast<T>(v_ptr);

}

template <typename T> void utilities::get_value_from_user(std::string msg, T& val){
std::string cmd;

std::cout << msg << ": ";

std::getline(std::cin, cmd);
stn(val,cmd);
}


template <typename T> void utilities::get_value_from_user(std::string msg, T* val){
std::string cmd;

std::cout << msg << ": ";

std::getline(std::cin, cmd);
stn(*val,cmd);
}



#endif
