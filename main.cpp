#include "0_UTILITIES/Includes.h"
#include <iostream>
#include COMMAND_LINE_INTERFACE

int main() {
command_info_container* C = new command_info_container;
cli::display_welcome_message(C);

            if (settings::get().require_password_on_startup) {cli::run_command_junction_C_CHANGE_USER(C);}
    while (settings::get().quit_flag == false) {cli::run_command_junction_C_MAIN(C);}

    cli::display_completion_message(C); std::cin.get();

    return 0;
}
