#pragma once

#include LIBS
#include DEFINES
#include ENUMS
#include TYPEDEFS

struct command_info_container{
    void* container_1;
    void* container_2;
};

class command;

class user{
public:
    user(){}
    user(std::string pw):password(pw){}
    user(std::string pw,std::map<std::string,int> cp):password(pw),command_permissions(cp){}
    std::string password;
    std::map<std::string,int> command_permissions;
};



namespace cli{

void run_authorisation(command_info_container*);

void display_welcome_message(command_info_container*);
void display_completion_message(command_info_container*);

void run_command_junction(COMMAND_JUNCTION_KEY key,command_info_container* user_data);
void display_available_commands(COMMAND_JUNCTION_KEY key);
std::string prompt_for_command(std::string message_to_user_if_different_from_default);
command match_input_text_to_a_command(std::string cmd,COMMAND_JUNCTION_KEY key);

/// commands
void exit_program(command_info_container* info_to_pass);

void run_command_junction_C_MAIN(command_info_container* info_to_pass);
void run_command_junction_C_QUIT(command_info_container* info_to_pass);
void run_command_junction_C_GO_BACK(command_info_container* info_to_pass);
void run_command_junction_C_TEST(command_info_container* info_to_pass);
void run_command_junction_C_CHANGE_USER(command_info_container* info_to_pass);

void test_code_snippet(command_info_container* info_to_pass);

}



struct command{
    command(){}
    command(std::vector<std::string> inv, std::string desc,ptr_to_command_func p,ptr_to_command_func pn = &cli::run_command_junction_C_MAIN,std::string w = EMPTY_STRING,std::string cm = EMPTY_STRING):
        text_of_command(inv),description_of_command(desc),func_to_execute(p),next_func_to_execute(pn){}
    command(std::string inv, std::string desc,ptr_to_command_func p,ptr_to_command_func pn = &cli::run_command_junction_C_MAIN,std::string w = EMPTY_STRING,std::string cm = EMPTY_STRING):
        description_of_command(desc),func_to_execute(p),next_func_to_execute(pn){text_of_command.push_back(inv);}

std::vector<std::string> text_of_command;
std::string description_of_command;

ptr_to_command_func func_to_execute = nullptr;
ptr_to_command_func next_func_to_execute = nullptr;

};

class settings{
settings();
    map_of_command_junctions command_storage;
    std::map<std::string,user> usernames_passwords_privileges;

public:
    static settings& get(){static settings S; return S;}

    bool perform_authorisation(std::string username,std::string password);
    void set_active_user(std::string username);

    void populate_available_usernames_and_passwords();
    void initialise_command_tree();
    void apply_permissions_to_command_availability();
    void add_auto_commands_to_junction(COMMAND_JUNCTION_KEY key);
    void add_numerical_command_invokes(COMMAND_JUNCTION_KEY key);
    command match_input_text_to_a_command(std::string cmd,COMMAND_JUNCTION_KEY key);


    bool quit_flag;
    bool require_password_on_startup;
    bool use_admin_permissions_by_default;

    map_of_command_junctions permissioned_command_tree;

    std::string active_user;

    COMMAND_JUNCTION_KEY previous_junction;
    COMMAND_JUNCTION_KEY current_junction;

    command cmd_J1_quit_confirmation;
    command cmd_J0_main_menu;
    command cmd_J1_go_back;
    command cmd_login;
};
