#pragma once

#include LIBS


class command_info_container;

/// WRITE ACTUAL APP CODE BELOW

struct test_input{
test_input(){}
test_input(void*){}
int intvar;
double q;
std::string stringvar;
};


struct test_output{
test_output(){}
int irr;
};


namespace tester{

test_output test(test_input t);

}


/// associated commands for these functions
namespace cli{

void load_test_input_xls(command_info_container* info_to_pass);
void load_test_input_csv(command_info_container* info_to_pass);
void prompt_for_single_test_inputs(command_info_container* info_to_pass);
void run_test(command_info_container* info_to_pass);

}
