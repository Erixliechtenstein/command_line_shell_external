#pragma once

/// UTILITIES
#define INCLUDES                            "0_UTILITIES/Includes.h"
#define TYPEDEFS                            "0_UTILITIES/Typedefs.h"
#define DEFINES                             "0_UTILITIES/Defines.h"
#define ENUMS                               "0_UTILITIES/Enums.h"
#define LIBS                                "0_UTILITIES/Libs.h"
#define UTILITIES                           "0_UTILITIES/Utilities.h"
#define UTILITIES_TEMPLATED_IMPL            "../src/0_UTILITIES/utilities_templated.cpp"

/// LIBRARIES
#define CSVH                                "0_UTILITIES/csv.h"
#define ENUMSTRING                          "0_UTILITIES/enumstring.h"

/// PROJECT SECTION 1
#define COMMAND_LINE_INTERFACE              "1_COMMAND_LINE/command_line_interface.h"

/// PROJECT SECTION 2 - ACTUAL APP
#define ACTUAL_APP_1                        "2_ACTUAL_APP/actual_app_includes.h"

