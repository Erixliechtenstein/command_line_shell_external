#pragma once

#include ENUMSTRING

/// ENUM 1 - []
enum READ_OR_WRITE {
READ     = 0,
WRITE    = 1,
};

Begin_Enum_String( READ_OR_WRITE ) {
    Enum_String(READ);
    Enum_String(WRITE);
}
End_Enum_String;




/// ENUM 2 - command keys
enum COMMAND_JUNCTION_KEY {
C_MAIN             = 0,
C_QUIT      = 1,
C_GO_BACK = 2,
C_TEST = 3,
C_CHANGE_USER = 4,
};

Begin_Enum_String( COMMAND_JUNCTION_KEY ) {
    Enum_String(C_MAIN);
    Enum_String(C_QUIT);
    Enum_String(C_GO_BACK);
    Enum_String(C_TEST);
    Enum_String(C_CHANGE_USER);
}
End_Enum_String;
