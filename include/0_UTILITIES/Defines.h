#pragma once

#define prnt std::cout <<
#define nextline std::cout << std::endl;
#define vlahs " | "
#define marker(x) std::cout << " | MARKER " << x << std::endl;
#define loop(x) for (int i = 0; i < x; ++i)

#define PRINT_FUNC_NAME_M() std::cout << "in function: " << __PRETTY_FUNCTION__ << std::endl;
#define PRINT_FUNC_NAME_P(b) if (b > 0) {std::cout << "in function: " << __PRETTY_FUNCTION__ << std::endl;}

#define stringify(varName,holder) sprintf(holder, "%s", #varName)
#define SIGN(v) ( ( (v) < 0 ) ? -1 : ( (v) > 0 ) )

#define FWDSLASH '/'
#define BACKSLASH '\\'
#define DASH '-'
#define BLANK ''
#define SPACE ' '
#define UNDERSCORE '_'
#define COMMA ","
#define DOT "."
#define divider " | "
#define EMPTY_STRING ""

#define UNDEFINED_STRING "UNDEFINED_STRING"
#define UNDEFINED_DOUBLE -9999.00
#define UNDEFINED_INT -9999
#define MAX_TREE_LINES 1000000
#define UNDEFINED_POSIX_TIME "2000-01-01 00:00:00.000"

#define DEFAULT_PROMPT_FOR_COMMAND_MESSAGE "Please enter command: "
