#pragma once

#include <windows.h>
#include <sys/stat.h>
#include <unistd.h>

#include LIBS
#include DEFINES
#include ENUMS

namespace utilities{

std::string get_right_of_last_delim(const std::string& str,const std::string& delim);
std::vector<std::string> split(const std::string& str, const std::string& delimiter);
std::string replace_string(std::string container, const std::string& what_to_replace,const std::string& replacement);
std::string to_lower_case(const std::string& uppercase);
std::string cut_slash_from_dir(const std::string& dir);
std::string cut_one_folder_level_from_dir(const std::string& dir);

bool        contains_string(const std::string& container, const std::string& containee);
bool        does_vector_contain(const std::vector<std::string>& container, const std::string& containee);
bool        is_string_empty(const std::string& s);
bool        is_string_empty_or_undefined(const std::string& s);
bool        does_file_exist(const std::string& full_file_path);
bool        does_directory_exist(const std::string& dir);

std::string current_directory();
std::string current_date_time();

std::string create_ffp(const std::string& file_name_or_folder, const std::string& dir_level_1, const std::string& dir_level_2 = "",const std::string& dir_level_3 = "");
std::string find_file_by_foldername_and_filename(const std::string& full_file_name, const std::string& folder, bool use_current_dir_as_search_base = true, const std::string& override_dir = "");
std::string find_folder_by_foldername(std::string folder, bool use_current_dir_as_search_base = true,std::string override_dir = "");


void stn(int& val,const std::string& Text); // stn - String To Number
void stn(float& val,const std::string& Text);
void stn(double& val,const std::string& Text);
void stn(bool& val,const std::string& Text);
void stn(std::string& val,const std::string& Text);

template <typename T> void get_vector_element(T save_to, int i, const std::vector<T>& v);
template <typename T> int find_nth_substring(int n, const std::basic_string<T>& container,const std::basic_string<T>& query,bool repeats = false);
template <typename T> void parse(T val, std::string varname, const std::vector<std::string>& line,const int id = UNDEFINED_INT);

template <typename T> void retrieve_void_ptr_val(T& val, void* v_ptr);

template <typename T> void get_value_from_user(std::string msg, T& val);
template <typename T> void get_value_from_user(std::string msg, T* val);
}

#include UTILITIES_TEMPLATED_IMPL

