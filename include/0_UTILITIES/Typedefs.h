#pragma once

#include ENUMS

class command_info_container;
class command;

typedef void  (*ptr_to_command_func) (command_info_container*);
typedef std::map<COMMAND_JUNCTION_KEY,std::vector<command>> (map_of_command_junctions);
